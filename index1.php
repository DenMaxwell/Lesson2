<?php

$Variable = 1;
echo "\$Variable is integer: $Variable";
echo "\n";

$Variable = 1.2;
echo "\$Variable is float: $Variable";
echo "\n";

$Variable = true;
echo "\$Variable is boolean: $Variable";
echo "\n";

$Variable = 'тук';
echo "\$Variable is string: $Variable";
echo "\n";

$Variable = 0B1001;
echo "\$Variable is 16: $Variable";
echo "\n";

?>